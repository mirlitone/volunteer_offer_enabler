jQuery(document).ready(function($){
	
	function updateStars(node){
		
		
		
			var stars=node.find("input:checked").length;
			var unstar=node.find("input:not(:checked)").length
			
			node.find("input:checked").parent().addClass("validated_knowhow");
			node.find("input:not(:checked)").parent().removeClass("validated_knowhow");
      
      
			
			var rainbow = new Rainbow(); 
			rainbow.setNumberRange(1, stars+unstar+1);
			rainbow.setSpectrum('yellow', "green");

			var newColor=rainbow.colourAt(stars+1);
			node.find("legend").attr("class","star"+stars);
			node.parent().css("background-color","#"+newColor);
			
		
	};
	
	
    $("input").each(function(){
        var type=(Math.floor(Math.random()*5)+1);
        $(this).parent().addClass("type-"+type);
      });


	$(".latent_knowhow").find(".form-item input:not(:checked)").parent().hide();
	$(".latent_knowhow").find(".form-item input:not(:checked)").parent().hide();
	
	$(".latent_knowhow").find(".form-item select").each(function(){
			if($(this).val()==0){
				$(this).parent().hide();
			}
		});
	$(".latent_knowhow div.fieldset-wrapper").each(function(){
		
		$(this).children("div.form-type-checkbox").first().show();
		});
	
	$(".latent_knowhow").find(".form-item input:checked").parent().next().show();
	
	$(".latent_knowhow").find("input").click(function() {
		if($(this).is(":checked",true)){//check
			$(this).parent().next().show("drop");
			
		}
		else{//uncheck
			$(this).parent().nextAll().children("input").attr("checked",false);
			$(this).parent().nextAll().hide("drop");
		}
		
		updateStars($(this).parent().parent());

		
	});
	
	$(".latent_knowhow div.fieldset-wrapper").each(
		function(){
			updateStars($(this))
		});
	

		
});
