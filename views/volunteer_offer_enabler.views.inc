<?php
/**
* @file
* View plugin volunteer offer module
*/


function volunteer_offer_enabler_views_data_alter(&$data) {
  $data['users']['skill_field'] = array(
    'title' => t('Skills field'),
    'help' => t('Skills for a user'),
    'filter' => array(
      'handler' => 'interval_select_handler_filter_interval',
    ),    
  );
}
